package org.grakovne.radio.tagger.exception;

public class FileConfigurationException extends RuntimeException {
    public FileConfigurationException(String message) {
        super(message);
    }
}
