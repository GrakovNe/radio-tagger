package org.grakovne.radio.tagger.exception;

public class Id3TagException extends RuntimeException {

    public Id3TagException() {
    }

    public Id3TagException(Throwable cause) {
        super(cause);
    }
}
