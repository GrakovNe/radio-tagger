package org.grakovne.radio.tagger.exception;

public class NativeLibraryException extends RuntimeException {

    public NativeLibraryException() {
    }

    public NativeLibraryException(String message) {
        super(message);
    }
}
