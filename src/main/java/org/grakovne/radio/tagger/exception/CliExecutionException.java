package org.grakovne.radio.tagger.exception;

public class CliExecutionException extends RuntimeException {

    public CliExecutionException() {
    }

    public CliExecutionException(String message) {
        super(message);
    }
}
