package org.grakovne.radio.tagger.domain;

import java.util.Objects;

public class FingerPrint {
    private Long duration;
    private String value;

    public FingerPrint() {
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FingerPrint that = (FingerPrint) o;
        return Objects.equals(duration, that.duration) &&
            Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(duration, value);
    }

    @Override
    public String toString() {
        return "FingerPrint{" +
            "duration=" + duration +
            ", value='" + value + '\'' +
            '}';
    }
}
