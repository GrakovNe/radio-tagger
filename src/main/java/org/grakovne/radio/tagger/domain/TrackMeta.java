package org.grakovne.radio.tagger.domain;

public class TrackMeta {
    private String title;
    private String release;
    private String date;
    private String artist;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    @Override
    public String toString() {
        return "TrackMeta{" +
            "title='" + title + '\'' +
            ", release='" + release + '\'' +
            ", date='" + date + '\'' +
            ", artist='" + artist + '\'' +
            '}';
    }
}
