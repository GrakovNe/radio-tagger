package org.grakovne.radio.tagger;

import org.grakovne.radio.tagger.id3.BatchProcessor;
import org.grakovne.radio.tagger.id3.BatchResult;

import java.io.File;
import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.out.println("Directory isn't specified");
            System.out.println("USAGE: java -jar <filename> <directory>");
            System.exit(1);
        }

        File directory = new File(args[0]);

        if (!directory.isDirectory()) {
            System.out.println("Path isn't correct");
            System.exit(1);
        }

        BatchResult result = new BatchProcessor().tagMp3Files(directory);

        System.out.println("");
        System.out.println("TAGGING FINISHED!");
        System.out.println("");
        System.out.println("PROCESSED FILES: " + result.getProcessedFiles().size());
        System.out.println("SUCCESSFUL FILES: " + result.getSuccessFiles().size());
        System.out.println("FAILURE FILES: " + result.getFailureFiles().size());
        System.out.println("");
        System.out.println("FAILURES IN: ");
        result.getFailureFiles().forEach(file -> System.out.println("\t" + file.getName()));
        System.out.println("FINISHING...");
    }
}
