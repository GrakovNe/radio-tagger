package org.grakovne.radio.tagger.network.api.musicbrainz;

import okhttp3.OkHttpClient;
import org.grakovne.radio.tagger.network.api.musicbrainz.dto.MusicBrainzResponse;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface MusicBrainzRepository {

    @GET
    Call<MusicBrainzResponse> getTrackMeta(@Url String url);

    class ApiFactory {
        public static MusicBrainzRepository create() {
            Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ApiNames.BASE_API)
                .client(new OkHttpClient())
                .build();

            return retrofit.create(MusicBrainzRepository.class);
        }
    }
}
