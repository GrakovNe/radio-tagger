package org.grakovne.radio.tagger.network.api.musicbrainz.dto;

public class MusicBrainzRelease {
    private String date;
    private String country;
    private String title;
    private String status;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "MusicBrainzRelease{" +
            "date='" + date + '\'' +
            ", country='" + country + '\'' +
            ", title='" + title + '\'' +
            ", status='" + status + '\'' +
            '}';
    }
}
