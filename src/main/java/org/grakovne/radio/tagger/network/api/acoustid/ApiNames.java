package org.grakovne.radio.tagger.network.api.acoustid;

public class ApiNames {
    public static final String BASE_API = "https://api.acoustid.org/v2/";
}
