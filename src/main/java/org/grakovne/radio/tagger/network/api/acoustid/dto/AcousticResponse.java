package org.grakovne.radio.tagger.network.api.acoustid.dto;

import java.util.List;

public class AcousticResponse {
    private String status;
    private List<AcousticResults> results;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<AcousticResults> getResults() {
        return results;
    }

    public void setResults(List<AcousticResults> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "AcousticResponse{" +
            "status='" + status + '\'' +
            ", results=" + results +
            '}';
    }
}
