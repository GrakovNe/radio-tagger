package org.grakovne.radio.tagger.network.api.acoustid;

import org.grakovne.radio.tagger.domain.FingerPrint;
import org.grakovne.radio.tagger.exception.ApiException;
import org.grakovne.radio.tagger.network.api.acoustid.dto.AcousticRecordings;
import org.grakovne.radio.tagger.network.api.acoustid.dto.AcousticResponse;
import org.grakovne.radio.tagger.network.api.acoustid.dto.AcousticResults;
import org.grakovne.radio.tagger.network.throttling.ThrottlingAgent;
import org.grakovne.radio.tagger.provider.ConfigurationProvider;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.Comparator;
import java.util.Optional;

public class AcousticIdService {

    private final AcousticIdRepository acousticIdRepository;
    private final ConfigurationProvider configurationProvider;
    private final ThrottlingAgent throttlingAgent;

    public AcousticIdService() {
        acousticIdRepository = AcousticIdRepository.ApiFactory.create();
        configurationProvider = new ConfigurationProvider();

        throttlingAgent = new ThrottlingAgent(10_000, 3);
    }

    public String identifyTrack(FingerPrint fingerPrint) {
        Call<AcousticResponse> request = acousticIdRepository
            .getTrackIds(
                fingerPrint.getValue(),
                fingerPrint.getDuration(),
                configurationProvider.getAcousticApiKey(),
                "recordingids");

        return throttlingAgent.resolve(() -> {
            try {
                Response<AcousticResponse> execute = request.execute();
                return findMostSimilarResult(Optional.ofNullable(execute.body())).getId();
            } catch (IOException e) {
                throw new ApiException();
            }
        });

    }

    private AcousticRecordings findMostSimilarResult(Optional<AcousticResponse> response) {
        return response
            .orElseThrow(ApiException::new)
            .getResults()
            .stream()
            .max(Comparator.comparing(AcousticResults::getScore))
            .orElseThrow(ApiException::new)
            .getRecordings()
            .stream()
            .findFirst()
            .orElseThrow(ApiException::new);
    }
}
