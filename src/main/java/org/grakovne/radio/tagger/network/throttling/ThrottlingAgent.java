package org.grakovne.radio.tagger.network.throttling;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import org.grakovne.radio.tagger.exception.ApiException;

import java.time.temporal.ChronoUnit;
import java.util.function.Supplier;

public class ThrottlingAgent {

    private Integer sleepTime;
    private Bucket throttlingBucket;

    public ThrottlingAgent(
        Integer sleepTime,
        Integer throttlingThreshold) {

        Bandwidth limit = Bandwidth.simple(
            throttlingThreshold,
            ChronoUnit.SECONDS.getDuration()
        );

        this.sleepTime = sleepTime;
        this.throttlingBucket = Bucket4j.builder().addLimit(limit).build();
    }

    public <T> T resolve(Supplier<T> supplier) {

        if (throttlingBucket.tryConsume(1)) {
            return supplier.get();
        } else {
            try {
                Thread.sleep(sleepTime);
                return resolve(supplier);
            } catch (InterruptedException e) {
                throw new ApiException();
            }
        }
    }
}