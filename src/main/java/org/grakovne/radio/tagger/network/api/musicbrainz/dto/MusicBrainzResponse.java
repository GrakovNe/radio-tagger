package org.grakovne.radio.tagger.network.api.musicbrainz.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MusicBrainzResponse {
    private String title;
    private String id;
    private String disambiguation;
    @SerializedName(value = "artist-credit")
    private List<MusicBrainzAtristCredit> artistCredit;
    private List<MusicBrainzRelease> releases;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisambiguation() {
        return disambiguation;
    }

    public void setDisambiguation(String disambiguation) {
        this.disambiguation = disambiguation;
    }

    public List<MusicBrainzAtristCredit> getArtistCredit() {
        return artistCredit;
    }

    public void setArtistCredit(List<MusicBrainzAtristCredit> artistCredit) {
        this.artistCredit = artistCredit;
    }

    public List<MusicBrainzRelease> getReleases() {
        return releases;
    }

    public void setReleases(List<MusicBrainzRelease> releases) {
        this.releases = releases;
    }

    @Override
    public String toString() {
        return "MusicBrainzResponse{" +
            "title='" + title + '\'' +
            ", id='" + id + '\'' +
            ", disambiguation='" + disambiguation + '\'' +
            ", artistCredit=" + artistCredit +
            ", releases=" + releases +
            '}';
    }
}
