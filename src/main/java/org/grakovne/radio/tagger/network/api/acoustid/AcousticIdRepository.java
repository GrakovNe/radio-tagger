package org.grakovne.radio.tagger.network.api.acoustid;

import okhttp3.OkHttpClient;
import org.grakovne.radio.tagger.network.api.acoustid.dto.AcousticResponse;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AcousticIdRepository {

    @FormUrlEncoded
    @POST("lookup")
    Call<AcousticResponse> getTrackIds(
        @Field("fingerprint") String fingerPrint,
        @Field("duration") Long duration,
        @Field("client") String apiKey,
        @Field("meta") String fetchingFields);

    class ApiFactory {
        public static AcousticIdRepository create() {
            Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ApiNames.BASE_API)
                .client(new OkHttpClient())
                .build();

            return retrofit.create(AcousticIdRepository.class);
        }
    }
}
