package org.grakovne.radio.tagger.network.api.acoustid.dto;

import java.util.List;

public class AcousticResults {
    private List<AcousticRecordings> recordings;
    private Double score;
    private String id;

    public List<AcousticRecordings> getRecordings() {
        return recordings;
    }

    public void setRecordings(List<AcousticRecordings> recordings) {
        this.recordings = recordings;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AcousticResults{" +
            "recordings=" + recordings +
            ", score=" + score +
            ", id='" + id + '\'' +
            '}';
    }
}
