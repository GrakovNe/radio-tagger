package org.grakovne.radio.tagger.network.api.musicbrainz.dto;

public class MusicBrainzAtristCredit {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MusicBrainzAtristCredit{" +
            "name='" + name + '\'' +
            '}';
    }
}
