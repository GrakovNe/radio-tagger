package org.grakovne.radio.tagger.network.api.acoustid.dto;

public class AcousticRecordings {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AcousticRecordings{" +
            "id='" + id + '\'' +
            '}';
    }
}
