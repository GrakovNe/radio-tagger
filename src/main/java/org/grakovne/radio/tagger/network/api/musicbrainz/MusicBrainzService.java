package org.grakovne.radio.tagger.network.api.musicbrainz;

import org.grakovne.radio.tagger.domain.TrackMeta;
import org.grakovne.radio.tagger.exception.ApiException;
import org.grakovne.radio.tagger.network.api.musicbrainz.dto.MusicBrainzAtristCredit;
import org.grakovne.radio.tagger.network.api.musicbrainz.dto.MusicBrainzRelease;
import org.grakovne.radio.tagger.network.api.musicbrainz.dto.MusicBrainzResponse;
import org.grakovne.radio.tagger.network.api.musicbrainz.dto.MusicBraizReleaseType;

import java.io.IOException;
import java.util.Optional;

public class MusicBrainzService {

    private final MusicBrainzRepository repository;

    public MusicBrainzService() {
        this.repository = MusicBrainzRepository.ApiFactory.create();
    }

    public TrackMeta fetchTrackData(String trackId) {
        MusicBrainzResponse response;

        try {
            response = repository.getTrackMeta(buildFetchingUrl(trackId)).execute().body();
            return buildTrackMeta(Optional.ofNullable(response));
        } catch (IOException e) {
            e.printStackTrace();
            throw new ApiException(e);
        }
    }

    private String buildFetchingUrl(String trackId) {
        return "recording/" + trackId + "?inc=artist-credits+releases&fmt=json";
    }

    private TrackMeta buildTrackMeta(Optional<MusicBrainzResponse> responseOptional) {
        MusicBrainzResponse response = responseOptional.orElseThrow(ApiException::new);

        TrackMeta result = new TrackMeta();

        String artistName = response
            .getArtistCredit()
            .stream()
            .findFirst()
            .orElse(new MusicBrainzAtristCredit())
            .getName();

        result.setArtist(artistName);

        MusicBrainzRelease release = response
            .getReleases()
            .stream()
            .filter(r -> r.getStatus().equalsIgnoreCase(MusicBraizReleaseType.OFFICIAL_TYPE))
            .findFirst()
            .orElse(response
                .getReleases()
                .stream()
                .findFirst()
                .orElse(new MusicBrainzRelease()));

        result.setRelease(release.getTitle());
        result.setDate(release.getDate());
        result.setTitle(response.getTitle());

        return result;
    }
}
