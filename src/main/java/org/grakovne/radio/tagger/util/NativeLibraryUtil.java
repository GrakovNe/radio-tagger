package org.grakovne.radio.tagger.util;

import org.grakovne.radio.tagger.domain.FingerPrint;
import org.grakovne.radio.tagger.exception.NativeLibraryException;

import java.io.File;

import static java.io.File.separator;

public class NativeLibraryUtil {

    private static final String DURATION_MARK = "DURATION=";
    private static final String FINGER_PRINT_MARK = "FINGERPRINT=";
    private static final String FP_CALC_MARK = "fpcalc";
    private static final String LIBS_DIRECTORY = "libs";

    public FingerPrint getFingerPrint(File audioFile) {
        if (null == audioFile || !audioFile.exists() || !audioFile.canRead()) {
            throw new IllegalArgumentException("file is not acceptable");
        }

        String rawResult = OperatingSystemUtil.runCliCommand(getFpCalcExecutable(), audioFile.getAbsolutePath());

        return parseRawFingerPrint(rawResult);
    }

    private FingerPrint parseRawFingerPrint(String rawValue) {

        if (null == rawValue || rawValue.isEmpty()) {
            throw new IllegalArgumentException();
        }

        FingerPrint fingerPrint = new FingerPrint();
        String rawDuration = rawValue.substring(rawValue.indexOf(DURATION_MARK) + DURATION_MARK.length(), rawValue.indexOf("\n"));
        String rawFingerPrint = rawValue.substring(rawValue.indexOf(FINGER_PRINT_MARK) + FINGER_PRINT_MARK.length());

        fingerPrint.setDuration(Long.valueOf(rawDuration));
        fingerPrint.setValue(rawFingerPrint);

        return fingerPrint;
    }

    private File getFpCalcExecutable() {
        String osType = OperatingSystemUtil.getOperatingSystemType();

        File file = new File(LIBS_DIRECTORY + separator + FP_CALC_MARK + separator + osType, FP_CALC_MARK);

        if (!file.exists() || !file.canExecute()) {
            throw new NativeLibraryException("Can't find fpcalc library for specified OS with path: " +
                file.getAbsolutePath());
        }

        return file;
    }
}
