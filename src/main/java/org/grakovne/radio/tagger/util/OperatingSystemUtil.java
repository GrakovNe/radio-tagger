package org.grakovne.radio.tagger.util;

import org.grakovne.radio.tagger.exception.CliExecutionException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class OperatingSystemUtil {

    private final static int SUCCESSFUL_PROCESS_CODE = 0;

    public static String getOperatingSystemType() {
        String osName = System.getProperty("os.name").replaceAll(" ", "").toLowerCase();
        String osArch = System.getProperty("os.arch").replaceAll(" ", "").toLowerCase();

        return osName + "_" + osArch;
    }

    public static String runCliCommand(File executiveFile, String arguments) {
        try {

            ProcessBuilder processBuilder = new ProcessBuilder(executiveFile.getAbsolutePath(), arguments);
            Process p = processBuilder.start();
            p.waitFor();

            if (p.exitValue() != SUCCESSFUL_PROCESS_CODE) {
                String errorResult = new BufferedReader(new InputStreamReader(p.getErrorStream())).readLine();
                throw new CliExecutionException(errorResult);
            }

            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder result = new StringBuilder(reader.readLine());

            String line;
            while ((line = reader.readLine()) != null) {
                result
                    .append("\n")
                    .append(line);
            }

            return result.toString();
        } catch (IOException | InterruptedException | NullPointerException ex) {
            ex.printStackTrace();
            throw new CliExecutionException();
        }
    }
}
