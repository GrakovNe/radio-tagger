package org.grakovne.radio.tagger.id3;

import org.grakovne.radio.tagger.domain.FingerPrint;
import org.grakovne.radio.tagger.domain.TrackMeta;
import org.grakovne.radio.tagger.network.api.acoustid.AcousticIdService;
import org.grakovne.radio.tagger.network.api.musicbrainz.MusicBrainzService;
import org.grakovne.radio.tagger.util.NativeLibraryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BatchProcessor {

    private final static Logger LOGGER = LoggerFactory.getLogger(BatchProcessor.class);

    public BatchResult tagMp3Files(File file) throws IOException {
        List<File> mp3Files = Files
            .walk(file.toPath())
            .filter(path -> isMp3File(path.toFile()))
            .map(Path::toFile)
            .collect(Collectors.toList());

        return tagMp3Files(mp3Files);
    }

    private BatchResult tagMp3Files(List<File> files) {
        BatchResult result = new BatchResult();

        files.forEach(file -> {
            try {
                NativeLibraryUtil nativeLibraryUtil = new NativeLibraryUtil();
                LOGGER.info("Processing file: {}", file.getName());
                FingerPrint fingerPrint = nativeLibraryUtil.getFingerPrint(file);

                AcousticIdService acousticIdService = new AcousticIdService();
                String trackId = acousticIdService.identifyTrack(fingerPrint);

                MusicBrainzService musicBrainzService = new MusicBrainzService();
                TrackMeta trackMeta = musicBrainzService.fetchTrackData(trackId);

                Id3TagsWriter writer = new Id3TagsWriter();
                writer.writeTrackMeta(file, trackMeta);
                result.MarkFileAsSuccess(file);

                LOGGER.info("Finished file: {}", file.getName());
            } catch (Exception ex) {
                LOGGER.info("Failure file: {}", file.getName());
                LOGGER.info("Failure by: {}", ex);
                result.MarkFileAsFailure(file);
            }
        });
        return result;
    }

    private boolean isMp3File(File file) {
        return file.getName().toLowerCase().endsWith(".mp3");
    }
}
