package org.grakovne.radio.tagger.id3;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;

public class BatchResult {

    private ConcurrentLinkedDeque<File> processedFiles;
    private ConcurrentLinkedDeque<File> successFiles;
    private ConcurrentLinkedDeque<File> failureFiles;

    public BatchResult() {
        processedFiles = new ConcurrentLinkedDeque<>();
        successFiles = new ConcurrentLinkedDeque<>();
        failureFiles = new ConcurrentLinkedDeque<>();
    }

    public synchronized void MarkFileAsSuccess(File file) {
        processedFiles.add(file);
        successFiles.add(file);
    }

    public synchronized void MarkFileAsFailure(File file) {
        processedFiles.add(file);
        failureFiles.add(file);
    }

    public List<File> getProcessedFiles() {
        return new ArrayList<>(processedFiles);
    }

    public List<File> getSuccessFiles() {
        return new ArrayList<>(successFiles);
    }

    public List<File> getFailureFiles() {
        return new ArrayList<>(failureFiles);
    }

    @Override
    public String toString() {
        return "BatchResult{" +
            "processedFiles=" + processedFiles +
            ", successFiles=" + successFiles +
            ", failureFiles=" + failureFiles +
            '}';
    }
}
