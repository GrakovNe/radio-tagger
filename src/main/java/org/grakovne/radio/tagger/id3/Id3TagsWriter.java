package org.grakovne.radio.tagger.id3;

import org.grakovne.radio.tagger.domain.TrackMeta;
import org.grakovne.radio.tagger.exception.Id3TagException;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldDataInvalidException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.id3.ID3v1Tag;
import org.jaudiotagger.tag.id3.ID3v24Tag;

import java.io.File;

public class Id3TagsWriter {

    public File writeTrackMeta(File file, TrackMeta meta) {

        if (null == file || !file.exists() || !file.canRead()) {
            throw new Id3TagException();
        }

        try {
            MP3File mp3File = (MP3File) AudioFileIO.read(file);

            mp3File = setId3v1Tags(mp3File, meta);
            mp3File = setId3v2Tags(mp3File, meta);

            mp3File.commit();
            mp3File.save();

        } catch (Exception e) {
            throw new Id3TagException(e);
        }
        return null;
    }

    private MP3File setId3v2Tags(MP3File mp3File, TrackMeta meta) {
        ID3v24Tag id3v2Tag = mp3File.getID3v2TagAsv24() == null ? new ID3v24Tag() : mp3File.getID3v2TagAsv24();

        setId3v2Value(id3v2Tag, FieldKey.ARTIST, meta.getArtist());
        setId3v2Value(id3v2Tag, FieldKey.TITLE, meta.getTitle());
        setId3v2Value(id3v2Tag, FieldKey.YEAR, meta.getDate());
        setId3v2Value(id3v2Tag, FieldKey.ALBUM, meta.getRelease());

        mp3File.setID3v2Tag(id3v2Tag);
        return mp3File;
    }

    private MP3File setId3v1Tags(MP3File mp3File, TrackMeta meta) {

        ID3v1Tag id3v1Tag = mp3File.getID3v1Tag() == null ? new ID3v1Tag() : mp3File.getID3v1Tag();

        setId3v1Value(id3v1Tag, FieldKey.ARTIST, meta.getArtist());
        setId3v1Value(id3v1Tag, FieldKey.TITLE, meta.getTitle());
        setId3v1Value(id3v1Tag, FieldKey.YEAR, meta.getDate());
        setId3v1Value(id3v1Tag, FieldKey.ALBUM, meta.getRelease());

        mp3File.setID3v1Tag(id3v1Tag);
        return mp3File;
    }


    private void setId3v2Value(ID3v24Tag tag, FieldKey key, String value) {
        try {
            tag.setField(key, value);
        } catch (FieldDataInvalidException ignored) {
        }
    }

    private void setId3v1Value(ID3v1Tag tag, FieldKey key, String value) {
        try {
            tag.setField(key, value);
        } catch (FieldDataInvalidException ignored) {
        }
    }
}
