package org.grakovne.radio.tagger.provider;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.grakovne.radio.tagger.exception.FileConfigurationException;

import java.io.File;

public class ConfigurationProvider {

    private final PropertiesConfiguration properties;
    private String acousticApiKey;

    public ConfigurationProvider() {

        Configurations configs = new Configurations();

        try {
            properties = configs.properties(new File("config.properties"));
        } catch (ConfigurationException cex) {
            throw new FileConfigurationException("Can't parse provider file");
        }

        initConfig();
    }

    private void initConfig() {
        acousticApiKey = properties.getString("acoustic_api_key");
    }

    public String getAcousticApiKey() {
        return acousticApiKey;
    }
}

